#include "calc/util/qt_util.h"

#include <QtGui/qfontdatabase.h>
#include <QtGui/qfontinfo.h>

namespace calc {
bool QtUtil::fontIsDefaultFont(const QFont& font) {
    return font == QtUtil::fontGetDefaultFont();
}

QFont QtUtil::fontGetDefaultFont() {
    return QFontDatabase::systemFont(QFontDatabase::GeneralFont);
}

int QtUtil::fontGetFontPointSize(const QFont& font) {
    QFontInfo info(font);
    return info.pointSize();
}
};  // namespace calc