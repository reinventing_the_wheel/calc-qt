#include <QtGui/qfontdatabase.h>
#include <calc/util/app_config.h>
#include <fmt/core.h>
#include <qforeach.h>

#include <QCoreApplication>
#include <QMutex>
#include <cstdlib>
#include <pugixml.hpp>
#include <string>

#include "calc/util/qt_util.h"
namespace calc {
AppConfig* AppConfig::app_config_self = nullptr;

AppConfig::AppConfig() {
    // app 版本信息
    this->m_app_version_major = static_cast<int>(strtol(CALC_VERSION_STR2(CALC_VERSION_MAJOR), nullptr, 10));
    this->m_app_version_minor = static_cast<int>(strtol(CALC_VERSION_STR2(CALC_VERSION_MINOR), nullptr, 10));
    this->m_app_version_patch = static_cast<int>(strtol(CALC_VERSION_STR2(CALC_VERSION_PATCH), nullptr, 10));
    this->m_app_version =
        QString::fromStdString(fmt::format("{}.{}.{}", this->m_app_version_major, this->m_app_version_minor, this->m_app_version_patch));

    // 应用程序所在目录
    this->m_app_dir = QDir(QCoreApplication::applicationDirPath());

    // 日志级别
#ifdef CALC_LOG_LEVEL
    // 定义映射关系
    const std::map<int, spdlog::level::level_enum> levelMap{
        {0, spdlog::level::off}, {1, spdlog::level::debug},    {2, spdlog::level::info}, {3, spdlog::level::warn},
        {4, spdlog::level::err}, {5, spdlog::level::critical}, {6, spdlog::level::trace}};

    // 根据宏定义的值获取相应的日志级别
    m_log_level = levelMap.at(CALC_LOG_LEVEL);
#else
    m_log_level = spdlog::level::info;
#endif

    // 配置文件所在目录
    auto dir = this->m_app_dir;
    dir.cdUp();
    this->m_config_file_path = dir.absolutePath() + "/config/config.xml";

    this->loadConfigFile();
}

static std::string configFileReadProperty(const pugi::xml_document& doc, const char* query, const std::string& defaultVal) {
    auto node = doc.select_node(query);
    if (!node) {
        return defaultVal;
    }
    auto        attr = node.node().attribute("value");
    std::string val(attr.value());
    if (!attr || val.empty()) {
        return defaultVal;
    }
    return attr.as_string();
}

void AppConfig::loadConfigFile() {
    // 加载配置文件
    pugi::xml_document doc;
    auto xmlLoadResult = doc.load_file(this->m_config_file_path.toStdString().c_str(), pugi::parse_default, pugi::encoding_utf8);
    // TODO 错误处理
    if (pugi::xml_parse_status::status_ok != xmlLoadResult.status) {
        this->saveConfigFile();
        return;
    }

    // 退出时保存模式
    this->m_config_behavior_save_model_on_exist =
        "on" == configFileReadProperty(doc, "/config/behavior/property[@name='save_model_on_exist']", "on");
    // 退出时保存模式
    this->m_config_behavior_save_define_on_exist =
        "on" == configFileReadProperty(doc, "/config/behavior/property[@name='save_define_on_exist']", "on");
    // 退出时清除历史记录
    this->m_config_behavior_clear_history_on_exist =
        "on" == configFileReadProperty(doc, "/config/behavior/property[@name='clear_history_on_exist']", "on");
    // 允许多个实例
    this->m_config_behavior_multi_instance = "on" == configFileReadProperty(doc, "/config/behavior/property[@name='multi_instance']", "on");

    // 记住窗口位置
    this->m_config_appearance_remember_window_position =
        "on" == configFileReadProperty(doc, "/config/appearance/property[@name='remember_window_position']", "off");
    // 窗口标题
    this->m_config_appearance_window_title =
        QString::fromStdString(configFileReadProperty(doc, "/config/appearance/property[@name='window_title']", "appname"));
    // 表达式行数
    this->m_config_appearance_expression_line = static_cast<int>(
        strtol(configFileReadProperty(doc, "/config/appearance/property[@name='expression_line']", "3").c_str(), nullptr, 10));

    this->initFontConfig();

    // 文字颜色
    this->m_config_font_color_color_word.setNamedColor(
        QString::fromStdString(configFileReadProperty(doc, "/config/font_color/property[@name='font_status']", "#000000")));
    // 错误状态颜色
    this->m_config_font_color_color_error.setNamedColor(
        QString::fromStdString(configFileReadProperty(doc, "/config/font_color/property[@name='color_error']", "#7F0000")));
    // 警告状态颜色
    this->m_config_font_color_color_warn.setNamedColor(
        QString::fromStdString(configFileReadProperty(doc, "/config/font_color/property[@name='color_warn']", "#0000B2")));
}

static void configFileWriteProperty(const pugi::xml_document& doc, const char* query, const std::string& val) {
    auto node = doc.select_node(query);
    // TODO 错误处理
    if (!node) {
        return;
    }
    node.node().attribute("value").set_value(val.c_str());
}

void AppConfig::saveConfigFile() {
    // 加载配置文件
    pugi::xml_document doc;
    auto xmlLoadResult = doc.load_file(this->m_config_file_path.toStdString().c_str(), pugi::parse_default, pugi::encoding_utf8);
    // TODO 错误处理
    if (pugi::xml_parse_status::status_ok != xmlLoadResult.status) {
        return;
    }
    // 退出时保存模式
    configFileWriteProperty(doc, "/config/behavior/property[@name='save_model_on_exist']",
                            this->configBehaviorSaveModelOnExist() ? "on" : "off");
    // 退出时保存定义
    configFileWriteProperty(doc, "/config/behavior/property[@name='save_define_on_exist']",
                            this->configBehaviorSaveDefineOnExist() ? "on" : "off");
    // 退出时清除历史记录
    configFileWriteProperty(doc, "/config/behavior/property[@name='clear_history_on_exist']",
                            this->configBehaviorClearHistoryOnExist() ? "on" : "off");
    // 允许多个实例
    configFileWriteProperty(doc, "/config/behavior/property[@name='multi_instance']", this->configBehaviorMultiInstance() ? "on" : "off");
    // 记住窗口位置
    configFileWriteProperty(doc, "/config/appearance/property[@name='remember_window_position']",
                            this->configAppearanceRememberWindowPosition() ? "on" : "off");
    // 窗口标题
    configFileWriteProperty(doc, "/config/appearance/property[@name='window_title']", this->configAppearanceWindowTitle().toStdString());
    // 表达式行数
    configFileWriteProperty(doc, "/config/appearance/property[@name='expression_line']",
                            std::to_string(this->configAppearanceExpressionLine()));
    // 结果字体
    configFileWriteProperty(doc, "/config/font_color/property[@name='font_result']",
                            this->configFontColorFontResult().toString().toStdString());
    // 表达式字体
    configFileWriteProperty(doc, "/config/font_color/property[@name='font_expression']",
                            this->configFontColorFontExpression().toString().toStdString());
    // 状态字体
    configFileWriteProperty(doc, "/config/font_color/property[@name='font_status']",
                            this->configFontColorFontStatus().toString().toStdString());
    // 文字颜色
    configFileWriteProperty(doc, "/config/font_color/property[@name='color_word']", this->configFontColorColorWord().name().toStdString());
    // 错误状态颜色
    configFileWriteProperty(doc, "/config/font_color/property[@name='color_error']",
                            this->configFontColorColorError().name().toStdString());
    // 警告状态颜色
    configFileWriteProperty(doc, "/config/font_color/property[@name='color_warn']", this->configFontColorColorWarn().name().toStdString());
}

AppConfig* AppConfig::getInstance() {
    if (app_config_self == nullptr) {
        static QMutex mutex;

        QMutexLocker locker(&mutex);

        if (app_config_self == nullptr) {
            app_config_self = new AppConfig;
        }
    }

    return app_config_self;
}

void AppConfig::initFontConfig() {
    // 加载配置文件
    pugi::xml_document doc;
    auto xmlLoadResult = doc.load_file(this->m_config_file_path.toStdString().c_str(), pugi::parse_default, pugi::encoding_utf8);
    // TODO 错误处理
    if (pugi::xml_parse_status::status_ok != xmlLoadResult.status) {
        return;
    }
    // 结果字体
    this->m_config_font_color_font_result.fromString(QString::fromStdString(configFileReadProperty(
        doc, "/config/font_color/property[@name='font_result']", QtUtil::fontGetDefaultFont().toString().toStdString())));
    // 表达式字体
    this->m_config_font_color_font_expression.fromString(QString::fromStdString(configFileReadProperty(
        doc, "/config/font_color/property[@name='font_expression']", QtUtil::fontGetDefaultFont().toString().toStdString())));
    // 状态字体
    this->m_config_font_color_font_status.fromString(QString::fromStdString(configFileReadProperty(
        doc, "/config/font_color/property[@name='font_status']", QtUtil::fontGetDefaultFont().toString().toStdString())));
}

void AppConfig::applyConfig(const QMap<QString, QString>& configMap) {
    if (configMap.empty()) {
        return;
    }
    // 加载配置文件
    pugi::xml_document doc;
    auto xmlLoadResult = doc.load_file(this->m_config_file_path.toStdString().c_str(), pugi::parse_default, pugi::encoding_utf8);
    // TODO 错误处理
    if (pugi::xml_parse_status::status_ok != xmlLoadResult.status) {
        return;
    }
    for (const auto& pair : configMap.toStdMap()) {
        auto node = doc.select_node(pair.first.toStdString().c_str());
        if (!node) {
            continue;
        }
        node.node().attribute("value").set_value(pair.second.toStdString().c_str());
    }
    doc.save_file(this->m_config_file_path.toStdString().c_str(), "\t", pugi::format_default & pugi::format_indent);
}
};  // namespace calc