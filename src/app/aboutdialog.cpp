#include "calc/app/aboutdialog.h"

#include <QFontDatabase>
#include <QLabel>
#include <QPixmap>
#include <QVBoxLayout>

#include "calc/util/app_config.h"

namespace calc {

AboutDialog::AboutDialog(QWidget *parent) : QDialog(parent) {
    this->setFixedSize(300, 250);
    this->setMinimumSize(300, 250);
    this->setWindowTitle("关于 calc");
    initLayout();
}

void AboutDialog::initLayout() {
    auto root_layout = new QVBoxLayout();
    root_layout->setAlignment(Qt::AlignCenter);
    this->setLayout(root_layout);

    auto    logo_label = new QLabel();
    QPixmap logo_png(":/img/logo");
    logo_label->setPixmap(logo_png);
    root_layout->addWidget(logo_label);
    logo_label->setAlignment(Qt::AlignCenter);

    auto label_appname = new QLabel("<b>calc-qt</>");
    root_layout->addWidget(label_appname);
    label_appname->setAlignment(Qt::AlignCenter);

    auto label_version = new QLabel(AppConfig::getInstance()->appVersion());
    root_layout->addWidget(label_version);
    label_version->setAlignment(Qt::AlignCenter);

    auto label_desc = new QLabel("基于词法分析与语法分析的简单计算器");
    root_layout->addWidget(label_desc);
    label_desc->setAlignment(Qt::AlignCenter);

    auto label_repo = new QLabel();
    label_repo->setOpenExternalLinks(true);
    label_repo->setTextFormat(Qt::RichText);
    label_repo->setText("<a href=\"https://gitlab.com/reinventing_the_wheel/calc-qt\">代码仓库</a>");
    root_layout->addWidget(label_repo);
    label_repo->setAlignment(Qt::AlignCenter);

    auto label_tip = new QLabel("本程序不包含任何担保");
    auto font      = QFontDatabase::systemFont(QFontDatabase::GeneralFont);
    font.setItalic(true);
    font.setPixelSize(12);
    label_tip->setFont(font);
    root_layout->addWidget(label_tip);
    label_tip->setAlignment(Qt::AlignCenter);
}

};  // namespace calc