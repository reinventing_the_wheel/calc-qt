#include "calc/app/mainwindow.h"

#include <QMenuBar>
#include <QMessageBox>

#include "calc/app/aboutdialog.h"
#include "calc/app/preferencedialog.h"
#include "calc/util/log_util.h"
namespace calc {
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    this->log = LogUtil::getLogger("mainwindow");
    SPDLOG_LOGGER_INFO(log, fmt::format("hello calc"));
    resize(500, 300);

    this->initMenu();
}

static std::string bool2string(bool val) {
    return val ? "true" : "false";
}

void printAppConfig() {
    std::shared_ptr<spdlog::logger> log       = calc::LogUtil::getLogger("app");
    auto                            appConfig = calc::AppConfig::getInstance();
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[appVersion]:{}", appConfig->appVersion().toStdString()));

    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configBehaviorSaveModelOnExist]:{}", bool2string(appConfig->configBehaviorSaveModelOnExist())));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configBehaviorSaveDefineOnExist]:{}", bool2string(appConfig->configBehaviorSaveDefineOnExist())));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configBehaviorClearHistoryOnExist]:{}", bool2string(appConfig->configBehaviorClearHistoryOnExist())));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configBehaviorMultiInstance]:{}", bool2string(appConfig->configBehaviorMultiInstance())));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[configAppearanceRememberWindowPosition]:{}",
                                         bool2string(appConfig->configAppearanceRememberWindowPosition())));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configAppearanceWindowTitle]:{}", appConfig->configAppearanceWindowTitle().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[configAppearanceExpressionLine]:{}", appConfig->configAppearanceExpressionLine()));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configFontColorFontResult]:{}", appConfig->configFontColorFontResult().toString().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[configFontColorFontExpression]:{}",
                                         appConfig->configFontColorFontExpression().toString().toStdString()));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configFontColorFontStatus]:{}", appConfig->configFontColorFontStatus().toString().toStdString()));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configFontColorColorWord]:{}", appConfig->configFontColorColorWord().name().toStdString()));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configFontColorColorError]:{}", appConfig->configFontColorColorError().name().toStdString()));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configFontColorColorWarn]:{}", appConfig->configFontColorColorWarn().name().toStdString()));
}

void MainWindow::initMenu() {
    QMenuBar *menubar = this->menuBar();
    this->setMenuBar(menubar);

    this->m_menu_file     = menubar->addMenu("文件");
    this->m_menu_edit     = menubar->addMenu("编辑");
    this->m_menu_model    = menubar->addMenu("模式");
    this->m_menu_func     = menubar->addMenu("函数");
    this->m_menu_variable = menubar->addMenu("变量");
    this->m_menu_unit     = menubar->addMenu("单位");
    this->m_menu_help     = menubar->addMenu("帮助");

    this->m_act_edit_copy_result = this->m_menu_edit->addAction("复制结果");
    this->m_menu_edit->addSeparator();
    this->m_act_edit_keys        = this->m_menu_edit->addAction("键盘快捷键");
    this->m_act_edit_custom_keys = this->m_menu_edit->addAction("自定义键盘快捷键");
    this->m_menu_edit->addSeparator();
    this->m_act_edit_preference = this->m_menu_edit->addAction("首选项");
    connect(this->m_act_edit_preference, &QAction::triggered, this, [this]() {
        if (nullptr == this->m_preference_dialog) {
            this->m_preference_dialog = new PreferenceDialog(this);
        }
        this->m_preference_dialog->exec();
    });

    this->m_act_help_content = this->m_menu_help->addAction("内容");
    connect(this->m_act_help_content, &QAction::triggered, this, []() {
        printAppConfig();
    });
    this->m_menu_help->addSeparator();
    this->m_act_help_report       = this->m_menu_help->addAction("报告错误");
    this->m_act_help_check_update = this->m_menu_help->addAction("检查更新");
    this->m_menu_help->addSeparator();
    this->m_act_help_about = this->m_menu_help->addAction("关于计算器");
    connect(this->m_act_help_about, &QAction::triggered, this, [this]() {
        if (nullptr == this->m_about_dialog) {
            this->m_about_dialog = new AboutDialog(this);
        }
        this->m_about_dialog->exec();
    });
    this->m_menu_help->addSeparator();
    this->m_act_help_about_qt = this->m_menu_help->addAction("关于 Qt");
    connect(this->m_act_help_about_qt, &QAction::triggered, [&]() {
        QMessageBox::aboutQt(nullptr);
    });
}

};  // namespace calc
