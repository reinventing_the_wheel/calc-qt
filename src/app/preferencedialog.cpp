#include "calc/app/preferencedialog.h"

#include <qobject.h>
#include <qpushbutton.h>

#include "calc/util/app_config.h"
#include "calc/util/log_util.h"
#include "calc/util/qt_util.h"

namespace calc {
void PreferenceDialogPanel::saveConfig() {
    AppConfig::getInstance()->applyConfig(m_config_changed_map);
    m_config_changed_map.clear();
}
bool PreferenceDialogPanel::hasConfigChanged() {
    return !m_config_changed_map.empty();
}

/* 行为选项配置面板 **************************************************************************************************************************/
PreferenceDialogBehaviorPanel::PreferenceDialogBehaviorPanel() {
    this->log = LogUtil::getLogger("PreferenceDialogBehaviorPanel");
    initLayout();
    initConnect();
}

void PreferenceDialogBehaviorPanel::initLayout() {
    auto layout_root = new QVBoxLayout;
    setLayout(layout_root);

    m_save_model_on_exist    = new QCheckBox("退出时保存模式");
    m_save_define_on_exist   = new QCheckBox("退出时保存定义");
    m_clear_history_on_exist = new QCheckBox("退出时清除历史记录");
    m_multi_instance         = new QCheckBox("允许多个实例");

    layout_root->addWidget(m_save_model_on_exist);
    layout_root->addWidget(m_save_define_on_exist);
    layout_root->addWidget(m_clear_history_on_exist);
    layout_root->addWidget(m_multi_instance);
}
void PreferenceDialogBehaviorPanel::initConnect() {
    QObject::connect(m_save_model_on_exist, &QCheckBox::stateChanged, this, [=](int state) {
        auto checked = state == Qt::Checked;
        SPDLOG_LOGGER_DEBUG(log, fmt::format("m_save_model_on_exist:{}", checked ? "on" : "off"));  // NOLINT
        this->m_config_changed_map.insert("/config/behavior/property[@name='save_model_on_exist']", checked ? "on" : "off");
    });
    QObject::connect(m_save_define_on_exist, &QCheckBox::stateChanged, this, [=](int state) {
        auto checked = state == Qt::Checked;
        SPDLOG_LOGGER_DEBUG(log, fmt::format("m_save_define_on_exist:{}", checked ? "on" : "off"));  // NOLINT
        this->m_config_changed_map.insert("/config/behavior/property[@name='save_define_on_exist']", checked ? "on" : "off");
    });
    QObject::connect(m_clear_history_on_exist, &QCheckBox::stateChanged, this, [=](int state) {
        auto checked = state == Qt::Checked;
        SPDLOG_LOGGER_DEBUG(log, fmt::format("m_clear_history_on_exist:{}", checked ? "on" : "off"));  // NOLINT
        this->m_config_changed_map.insert("/config/behavior/property[@name='clear_history_on_exist']", checked ? "on" : "off");
    });
    QObject::connect(m_multi_instance, &QCheckBox::stateChanged, this, [=](int state) {
        auto checked = state == Qt::Checked;
        SPDLOG_LOGGER_DEBUG(log, fmt::format("m_multi_instance:{}", checked ? "on" : "off"));  // NOLINT
        this->m_config_changed_map.insert("/config/behavior/property[@name='clear_history_on_exist']", checked ? "on" : "off");
    });
}
QString PreferenceDialogBehaviorPanel::tabTitle() {
    return "行为";
}
void PreferenceDialogBehaviorPanel::resetConfig() {
    auto config = AppConfig::getInstance();
    m_save_model_on_exist->setChecked(config->configBehaviorSaveModelOnExist());
    m_save_define_on_exist->setChecked(config->configBehaviorSaveDefineOnExist());
    m_clear_history_on_exist->setChecked(config->configBehaviorClearHistoryOnExist());
    m_multi_instance->setChecked(config->configBehaviorMultiInstance());

    m_config_changed_map.clear();
}

/* 外观选项配置面板 **************************************************************************************************************************/
PreferenceDialogAppearancePanel::PreferenceDialogAppearancePanel() {
    this->log = LogUtil::getLogger("PreferenceDialogAppearancePanel");

    m_title_val_index_map.insert("appname", 0);
    m_title_val_index_map.insert("result", 1);
    m_title_val_index_map.insert("appname_result", 2);
    m_title_val_index_map.insert("model", 3);
    m_title_val_index_map.insert("appname_model", 4);

    m_title_index_val_map.insert(0, "appname");
    m_title_index_val_map.insert(1, "result");
    m_title_index_val_map.insert(2, "appname_result");
    m_title_index_val_map.insert(3, "model");
    m_title_index_val_map.insert(4, "appname_model");

    initLayout();
    initConnect();
}

void PreferenceDialogAppearancePanel::initLayout() {
    auto layout_root = new QVBoxLayout;
    setLayout(layout_root);

    m_remember_window_position_checkbox = new QCheckBox("记住窗口位置");
    layout_root->addWidget(m_remember_window_position_checkbox);

    auto layout_title = new QHBoxLayout();
    layout_title->setAlignment(Qt::AlignLeft);
    layout_title->addWidget(new QLabel("窗口标题\u3000"));
    m_window_title_combobox = new QComboBox;
    m_window_title_combobox->addItem("应用程序名称");
    m_window_title_combobox->addItem("结果");
    m_window_title_combobox->addItem("应用程序名称+结果");
    m_window_title_combobox->addItem("模式");
    m_window_title_combobox->addItem("应用程序名称+模式");
    layout_title->addWidget(m_window_title_combobox);
    layout_root->addLayout(layout_title);

    auto layout_line = new QHBoxLayout;
    layout_line->setAlignment(Qt::AlignLeft);
    layout_line->addWidget(new QLabel("表达式行数"));
    m_expression_spinbox = new QSpinBox;
    m_expression_spinbox->setMinimum(1);
    m_expression_spinbox->setSingleStep(1);
    layout_line->addWidget(m_expression_spinbox);
    layout_root->addLayout(layout_line);
}
void PreferenceDialogAppearancePanel::initConnect() {
    QObject::connect(m_remember_window_position_checkbox, &QCheckBox::stateChanged, this, [=](int state) {
        auto checked = state == Qt::Checked;
        SPDLOG_LOGGER_DEBUG(log, fmt::format("remember_window_position:{}", checked ? "on" : "off"));  // NOLINT
        this->m_config_changed_map.insert("/config/appearance/property[@name='remember_window_position']", checked ? "on" : "off");
    });

    QObject::connect(m_window_title_combobox, &QComboBox::currentIndexChanged, this, [=](int index) {
        auto title = m_title_index_val_map[index].toStdString();
        SPDLOG_LOGGER_DEBUG(log, fmt::format("window_title:{}", m_title_index_val_map[index].toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/appearance/property[@name='window_title']", QString::fromStdString(title));
    });

    QObject::connect(m_expression_spinbox, &QSpinBox::valueChanged, this, [=](int val) {
        SPDLOG_LOGGER_DEBUG(log, fmt::format("expression_line:{}", val));  // NOLINT
        this->m_config_changed_map.insert("/config/appearance/property[@name='expression_line']", QString::number(val));
    });
}
QString PreferenceDialogAppearancePanel::tabTitle() {
    return "外观";
}
void PreferenceDialogAppearancePanel::resetConfig() {
    auto config = AppConfig::getInstance();
    m_remember_window_position_checkbox->setChecked(config->configAppearanceRememberWindowPosition());
    m_window_title_combobox->activated(m_title_val_index_map.value(config->configAppearanceWindowTitle()));
    m_expression_spinbox->setValue(config->configAppearanceExpressionLine());

    m_config_changed_map.clear();
}

/* 字体与颜色选项配置面板 *********************************************************************************************************************/
PreferenceDialogFontColorPanel::PreferenceDialogFontColorPanel() {
    this->log = LogUtil::getLogger("PreferenceDialogFontColorPanel");
    initLayout();
    initConnect();
}

void PreferenceDialogFontColorPanel::initLayout() {
    auto layout_root = new QVBoxLayout;
    setLayout(layout_root);

    m_check_font_result = new QCheckBox("自定义结果字体");
    m_btn_font_result   = new QPushButton("");
    layout_root->addWidget(m_check_font_result);
    layout_root->addWidget(m_btn_font_result);

    m_check_font_expression = new QCheckBox("自定义表达式字体");
    m_btn_font_expression   = new QPushButton("");
    layout_root->addWidget(m_check_font_expression);
    layout_root->addWidget(m_btn_font_expression);

    m_check_font_status = new QCheckBox("自定义状态字体");
    m_btn_font_status   = new QPushButton("");
    layout_root->addWidget(m_check_font_status);
    layout_root->addWidget(m_btn_font_status);

    auto layout_color_word = new QHBoxLayout;
    layout_color_word->setAlignment(Qt::AlignLeft);
    auto label_color_word = new QLabel("文字颜色\u3000\u3000");
    m_btn_color_word      = new QPushButton();
    layout_color_word->addWidget(label_color_word);
    layout_color_word->addWidget(m_btn_color_word);
    layout_root->addLayout(layout_color_word);

    auto layout_color_error = new QHBoxLayout;
    layout_color_error->setAlignment(Qt::AlignLeft);
    auto label_color_error = new QLabel("错误状态颜色");
    m_btn_color_error      = new QPushButton();
    layout_color_error->addWidget(label_color_error);
    layout_color_error->addWidget(m_btn_color_error);
    layout_root->addLayout(layout_color_error);

    auto layout_color_warn = new QHBoxLayout;
    layout_color_warn->setAlignment(Qt::AlignLeft);
    auto label_color_warn = new QLabel("警告状态颜色");
    m_btn_color_warn      = new QPushButton();
    layout_color_warn->addWidget(label_color_warn);
    layout_color_warn->addWidget(m_btn_color_warn);
    layout_root->addLayout(layout_color_warn);
}

void PreferenceDialogFontColorPanel::initConnect() {
    QObject::connect(m_check_font_result, &QCheckBox::stateChanged, this, [=](int state) {
        m_btn_font_result->setEnabled(state == Qt::Checked);
    });
    QObject::connect(m_btn_font_result, &QPushButton::clicked, this, [=]() {
        bool ok;
        auto font = QFontDialog::getFont(&ok);
        if (!ok) {
            this->m_config_changed_map.remove("/config/font_color/property[@name='font_result']");
            return;
        }
        SPDLOG_LOGGER_DEBUG(log, fmt::format("font_result:{}", font.toString().toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/font_color/property[@name='font_result']", font.toString());
    });
    QObject::connect(m_check_font_expression, &QCheckBox::stateChanged, this, [=](int state) {
        m_btn_font_expression->setEnabled(state == Qt::Checked);
    });
    QObject::connect(m_btn_font_expression, &QPushButton::clicked, this, [=]() {
        bool ok;
        auto font = QFontDialog::getFont(&ok);
        if (!ok) {
            this->m_config_changed_map.remove("/config/font_color/property[@name='font_expression']");
            return;
        }
        SPDLOG_LOGGER_DEBUG(log, fmt::format("font_result:{}", font.toString().toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/font_color/property[@name='font_expression']", font.toString());
    });
    QObject::connect(m_check_font_status, &QCheckBox::stateChanged, this, [=](int state) {
        m_btn_font_status->setEnabled(state == Qt::Checked);
    });
    QObject::connect(m_btn_font_status, &QPushButton::clicked, this, [=]() {
        bool ok;
        auto font = QFontDialog::getFont(&ok);
        if (!ok) {
            this->m_config_changed_map.remove("/config/font_color/property[@name='font_status']");
            return;
        }
        SPDLOG_LOGGER_DEBUG(log, fmt::format("font_result:{}", font.toString().toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/font_color/property[@name='font_status']", font.toString());
    });

    QObject::connect(m_btn_color_word, &QPushButton::clicked, this, [=]() {
        auto color = QColorDialog::getColor();
        if (!color.isValid()) {
            return;
        }
        SPDLOG_LOGGER_DEBUG(log, fmt::format("color_word:{}", color.name().toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/font_color/property[@name='color_word']", color.name());
        m_btn_color_word->setStyleSheet(QString::fromStdString(fmt::format("background-color:{}", color.name().toStdString())));
    });
    QObject::connect(m_btn_color_error, &QPushButton::clicked, this, [=]() {
        auto color = QColorDialog::getColor();
        if (!color.isValid()) {
            return;
        }
        SPDLOG_LOGGER_DEBUG(log, fmt::format("color_error:{}", color.name().toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/font_color/property[@name='color_error']", color.name());
        m_btn_color_error->setStyleSheet(QString::fromStdString(fmt::format("background-color:{}", color.name().toStdString())));
    });
    QObject::connect(m_btn_color_warn, &QPushButton::clicked, this, [=]() {
        auto color = QColorDialog::getColor();
        if (!color.isValid()) {
            return;
        }
        SPDLOG_LOGGER_DEBUG(log, fmt::format("color_warn:{}", color.name().toStdString()));  // NOLINT
        this->m_config_changed_map.insert("/config/font_color/property[@name='color_warn']", color.name());
        m_btn_color_warn->setStyleSheet(QString::fromStdString(fmt::format("background-color:{}", color.name().toStdString())));
    });
}
QString PreferenceDialogFontColorPanel::tabTitle() {
    return "字体与颜色";
}
void PreferenceDialogFontColorPanel::resetConfig() {
    auto config = AppConfig::getInstance();

    auto font_result = config->configFontColorFontResult();
    m_check_font_result->setChecked(!QtUtil::fontIsDefaultFont(font_result));
    m_btn_font_result->setText(QString::fromStdString(fmt::format("{} {}", font_result.family().toStdString(), font_result.pointSize())));
    m_btn_font_result->setEnabled(m_check_font_result->isChecked());

    auto font_expression = config->configFontColorFontExpression();
    m_check_font_expression->setChecked(!QtUtil::fontIsDefaultFont(font_expression));
    m_btn_font_expression->setText(
        QString::fromStdString(fmt::format("{} {}", font_expression.family().toStdString(), font_expression.pointSize())));
    m_btn_font_expression->setEnabled(m_check_font_expression->isChecked());

    auto font_status = config->configFontColorFontStatus();
    m_check_font_status->setChecked(!QtUtil::fontIsDefaultFont(font_status));
    m_btn_font_status->setText(QString::fromStdString(fmt::format("{} {}", font_status.family().toStdString(), font_status.pointSize())));
    m_btn_font_status->setEnabled(m_check_font_status->isChecked());

    m_btn_color_word->setStyleSheet(
        QString::fromStdString(fmt::format("background-color:{}", config->configFontColorColorWord().name().toStdString())));
    m_btn_color_error->setStyleSheet(
        QString::fromStdString(fmt::format("background-color:{}", config->configFontColorColorError().name().toStdString())));
    m_btn_color_warn->setStyleSheet(
        QString::fromStdString(fmt::format("background-color:{}", config->configFontColorColorWarn().name().toStdString())));

    m_config_changed_map.clear();
}

/* 首选项对话框 ******************************************************************************************************************************/
PreferenceDialog::PreferenceDialog(QWidget *parent) : QDialog(parent) {
    this->log = LogUtil::getLogger("PreferenceDialog");
    this->setWindowTitle("首选项");
    this->setMinimumHeight(180);
    this->setMinimumWidth(300);

    AppConfig::getInstance()->loadConfigFile();

    initLayout();
}

void PreferenceDialog::initLayout() {
    auto root_layout = new QVBoxLayout();
    root_layout->setAlignment(Qt::AlignCenter);
    this->setLayout(root_layout);

    m_panels.append(new PreferenceDialogBehaviorPanel());
    m_panels.append(new PreferenceDialogAppearancePanel());
    m_panels.append(new PreferenceDialogFontColorPanel());

    this->m_tab = new QTabWidget;
    for (const auto panel : m_panels) {
        panel->resetConfig();
        this->m_tab->addTab(panel, panel->tabTitle());
    }
    m_tab->setCurrentIndex(m_curr_tab_index);

    QMap<int, QString> tabTitleMap;
    tabTitleMap.insert(0, "行为");
    tabTitleMap.insert(1, "外观");
    tabTitleMap.insert(2, "字体与颜色");
    QObject::connect(this->m_tab, &QTabWidget::currentChanged, this, [=](int index) {
        auto prevPanel = m_panels[m_curr_tab_index];
        if (prevPanel->hasConfigChanged()) {
            int ret = QMessageBox::question(
                this, "提示", QString::fromStdString(fmt::format("{} 有未保存的设置, 是否保存?", tabTitleMap[index].toStdString())));
            if (ret == QMessageBox::Yes) {
                SPDLOG_LOGGER_DEBUG(log, fmt::format("save settings"));  // NOLINT
                prevPanel->saveConfig();
            } else {
                prevPanel->resetConfig();
            }
        }
        m_curr_tab_index = index;
    });

    root_layout->addWidget(this->m_tab);

    auto btn_layout = new QHBoxLayout;
    btn_layout->setAlignment(Qt::AlignRight);
    auto btn_apply = new QPushButton("应用");
    auto btn_reset = new QPushButton("重置");
    auto btn_close = new QPushButton("关闭");
    btn_layout->addWidget(btn_apply);
    btn_layout->addWidget(btn_reset);
    btn_layout->addWidget(btn_close);
    root_layout->addLayout(btn_layout);

    QObject::connect(btn_apply, &QPushButton::clicked, this, [=]() {
        foreach (const auto panel, m_panels) {
            panel->saveConfig();
            AppConfig::getInstance()->loadConfigFile();
        }
    });
    QObject::connect(btn_reset, &QPushButton::clicked, this, [=]() {
        foreach (const auto panel, m_panels) {
            panel->resetConfig();
        }
    });
    QObject::connect(btn_close, &QPushButton::clicked, this, &PreferenceDialog::close);
}

void PreferenceDialog::showEvent(QShowEvent *e) {
    QDialog::showEvent(e);

    if (m_panels.empty()) {
        return;
    }
    for (const auto panel : m_panels) {
        panel->resetConfig();
    }
}
};  // namespace calc
