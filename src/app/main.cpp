#include <qfontinfo.h>

#include <QApplication>
#include <QScreen>

#include "calc/app/mainwindow.h"
#include "calc/util/app_config.h"
#include "calc/util/log_util.h"

/**
 * @brief 打印程序配置信息
 */
void printAppConfig();

/**
 * @brief 窗口居中
 * @param w 主窗口实例
 */
void setWindowInCenter(calc::MainWindow &w);

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    calc::LogUtil::init(calc::AppConfig::getInstance()->logLevel(), "../logs/app.log");
    std::shared_ptr<spdlog::logger> log = calc::LogUtil::getLogger("app");

    calc::AppConfig::getInstance()->initFontConfig();

    printAppConfig();

    calc::MainWindow w;
    setWindowInCenter(w);
    w.show();
    return QApplication::exec();
}

static std::string bool2string(bool val) {
    return val ? "true" : "false";
}

void printAppConfig() {
    std::shared_ptr<spdlog::logger> log       = calc::LogUtil::getLogger("app");
    auto                            appConfig = calc::AppConfig::getInstance();
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[appVersion]:{}", appConfig->appVersion().toStdString()));

    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configBehaviorSaveModelOnExist]:{}", bool2string(appConfig->configBehaviorSaveModelOnExist())));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configBehaviorSaveDefineOnExist]:{}", bool2string(appConfig->configBehaviorSaveDefineOnExist())));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configBehaviorClearHistoryOnExist]:{}", bool2string(appConfig->configBehaviorClearHistoryOnExist())));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configBehaviorMultiInstance]:{}", bool2string(appConfig->configBehaviorMultiInstance())));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[configAppearanceRememberWindowPosition]:{}",
                                         bool2string(appConfig->configAppearanceRememberWindowPosition())));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configAppearanceWindowTitle]:{}", appConfig->configAppearanceWindowTitle().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[configAppearanceExpressionLine]:{}", appConfig->configAppearanceExpressionLine()));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configFontColorFontResult]:{}", appConfig->configFontColorFontResult().toString().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[configFontColorFontExpression]:{}",
                                         appConfig->configFontColorFontExpression().toString().toStdString()));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configFontColorFontStatus]:{}", appConfig->configFontColorFontStatus().toString().toStdString()));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configFontColorColorWord]:{}", appConfig->configFontColorColorWord().name().toStdString()));
    SPDLOG_LOGGER_DEBUG(
        log, fmt::format("appConfig[configFontColorColorError]:{}", appConfig->configFontColorColorError().name().toStdString()));
    SPDLOG_LOGGER_DEBUG(log,
                        fmt::format("appConfig[configFontColorColorWarn]:{}", appConfig->configFontColorColorWarn().name().toStdString()));
}

/**
 * 是否在主屏幕显示
 */
#define SHOW_WINDOW_IN_PRIMARY_SCREEN 1

void setWindowInCenter(calc::MainWindow &w) {
#if SHOW_WINDOW_IN_PRIMARY_SCREEN
    // 获取主屏幕的指针
    QScreen *primaryScreen = QGuiApplication::primaryScreen();
    // 获取主屏幕的可用工作区几何信息
    QRect availableGeometry = primaryScreen->availableGeometry();
#else
    // 获取当前屏幕的可用工作区几何信息
    QScreen *currentScreen     = QGuiApplication::screenAt(w.pos());
    QRect    availableGeometry = currentScreen->availableGeometry();
#endif
    // 计算窗口居中显示的位置
    int x = availableGeometry.x() + (availableGeometry.width() - w.width()) / 2;
    int y = availableGeometry.y() + (availableGeometry.height() - w.height()) / 2;

    // 移动窗口到居中的位置
    w.move(x, y);
}