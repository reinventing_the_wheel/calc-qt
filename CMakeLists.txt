cmake_minimum_required(VERSION 3.26)

# 语言环境配置 ###########################################################################################################
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# 项目配置 ##############################################################################################################
project(calc-qt VERSION 1.0.0 LANGUAGES CXX)
set(dist_dir ${CMAKE_BINARY_DIR}/dist)

# 二进制文件目录
set(bin_dir ${dist_dir}/bin)

# 启动文件目录
set(sbin_dir ${dist_dir}/sbin)

# 库文件目录
set(lib_dir ${dist_dir}/lib)

# 资源文件目录
set(res_dir ${dist_dir}/res)

# 配置文件目录
set(config_dir ${dist_dir}/config)

# 通用工具类库
set(util_lib_name util)

set(CMAKE_CXX_FLAGS "-Wall -Wextra")

find_package(PkgConfig REQUIRED)

# qt 库 ################################################################################################################
find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)

# vcpkg ################################################################################################################
# set(vckpg_dir "${CMAKE_SOURCE_DIR}/vcpkg")
# set(VCPKG_TARGET_TRIPLET "x64-linux-dynamic")
# set(CMAKE_TOOLCHAIN_FILE "${vckpg_dir}/scripts/buildsystems/vcpkg.cmake")
# set(fmt_DIR "${vckpg_dir}/packages/fmt_x64-linux-dynamic/share/fmt")
# set(spdlog_DIR "${vckpg_dir}/packages/spdlog_x64-linux-dynamic/share/spdlog")

# fmt 库
find_package(fmt CONFIG REQUIRED)
get_target_property(fmt_LOCATION fmt::fmt LOCATION)
get_filename_component(fmt_DIRECTORY ${fmt_LOCATION} DIRECTORY)

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    string(REPLACE "debug/" "" fmt_DIRECTORY ${fmt_DIRECTORY})
endif()

message(STATUS "fmt_DIRECTORY:${fmt_DIRECTORY}")

# spdlog 库
find_package(spdlog CONFIG REQUIRED)
get_target_property(spdlog_LOCATION spdlog::spdlog LOCATION)
get_filename_component(spdlog_DIRECTORY ${spdlog_LOCATION} DIRECTORY)

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    string(REPLACE "debug/" "" spdlog_DIRECTORY ${spdlog_DIRECTORY})
endif()

# 不需要 debug 日志时,注释下面两行
add_compile_options(-DSPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG)
add_compile_options(-DCALC_LOG_LEVEL=1)

message(STATUS "spdlog_DIRECTORY:${spdlog_DIRECTORY}")

# gmpxx
pkg_check_modules(gmpxx REQUIRED IMPORTED_TARGET gmpxx)

# pugixml
find_package(pugixml CONFIG REQUIRED)
get_target_property(pugixml_LOCATION pugixml::shared LOCATION)
get_filename_component(pugixml_DIRECTORY ${pugixml_LOCATION} DIRECTORY)

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    string(REPLACE "debug/" "" pugixml_DIRECTORY ${pugixml_DIRECTORY})
endif()

message(STATUS "pugixml_LOCATION:${pugixml_LOCATION}")

# 编译相关配置 ###########################################################################################################
# 资源文件
set(QRC_FILES ${PROJECT_SOURCE_DIR}/res.qrc)

# 项目配置文件
configure_file(calc_configuration.h.in ${PROJECT_SOURCE_DIR}/include/calc/util/configuration.h)

# 生成 compile_commands.json
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# 包含全局头文件
include_directories(${PROJECT_SOURCE_DIR}/include)

# 添加子目录 #############################################################################################################
add_subdirectory(src)

# 启用测试 ##############################################################################################################
enable_testing()
add_subdirectory(test)
