/**
 * @file log_util.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 日志工具类
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef CALC_LOG_UTIL_H_
#define CALC_LOG_UTIL_H_

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/daily_file_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/spdlog.h>

#include <string>
#include <vector>

#include "app_config.h"
namespace calc {
/**
 * @brief 日志工具类
 */
class LogUtil {
public:
    static void init(spdlog::level::level_enum level = AppConfig::getInstance()->logLevel(), std::string log_file = "app.log");
    static spdlog::level::level_enum     getGlobalLevel();
    static std::vector<spdlog::sink_ptr> createSinks(const std::string &log_file_name);
    static void createLogger(const std::string &logger_name, spdlog::level::level_enum level = AppConfig::getInstance()->logLevel());
    static std::shared_ptr<spdlog::logger> getLogger(const std::string        &logger_name,
                                                     spdlog::level::level_enum level = AppConfig::getInstance()->logLevel());

private:
    static spdlog::level::level_enum global_level;
    static std::string               log_file;
};
};  // namespace calc
#endif
