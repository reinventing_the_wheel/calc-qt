/**
 * @file calc_configuration.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 应用程序版本
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef CALC_CONFIGURATION_H_
#define CALC_CONFIGURATION_H_
namespace calc {
// clang-format off
/**
 * @brief 主版本号
 */
#define CALC_VERSION_MAJOR 1
/**
 * @brief 次版本号
 */
#define CALC_VERSION_MINOR 0
/**
 * @brief 修订版本号
 */
#define CALC_VERSION_PATCH 0
// clang-format on
};  // namespace calc
#endif
