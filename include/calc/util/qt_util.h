/**
 * @file qt_util.h
 * @author khl (xiaodaima2016@163.com)
 * @brief qt 工具类
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef CALC_QT_UTIL_H_
#define CALC_QT_UTIL_H_

#include <QObject>
#include <QtGui/QFont>

namespace calc {

/**
 * @brief qt 工具类
 */
class QtUtil : public QObject {
    Q_OBJECT
private:
    QtUtil()           = default;
    ~QtUtil() override = default;

public:
    /**
     * @brief 判断字体是否为默认字体
     * 
     * @param font 待判断的字体
     * @return true 默认字体
     * @return false 非默认字体
     */
    static bool fontIsDefaultFont(const QFont& font);
    /**
     * @brief 获取当前系统默认字体
     * 
     * @return QFont 当前系统默认字体
     */
    static QFont fontGetDefaultFont();
    /**
     * @brief 获取字体大小
     * 
     * @param font 字体
     * @return int 字体大小
     */
    static int fontGetFontPointSize(const QFont& font);
};

};  // namespace calc
#endif
