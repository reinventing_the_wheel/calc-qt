/**
 * @file app_config.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 应用程序配置
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef CALC_APP_CONFIG_H_
#define CALC_APP_CONFIG_H_

#include <QtGui/qfontdatabase.h>
#include <spdlog/spdlog.h>

#include <QDir>
#include <QObject>
#include <QString>
#include <QtGui/QColor>
#include <QtGui/QFont>
#include <QtGui/QFontDatabase>

#include "configuration.h"
namespace calc {
#define CALC_VERSION_MAJOR_COPY (CALC_VERSION_MAJOR)
#define CALC_VERSION_MINOR_COPY (CALC_VERSION_MINOR)
#define CALC_VERSION_PATCH_COPY (CALC_VERSION_PATCH)

#define CALC_VERSION_STR(R) #R
#define CALC_VERSION_STR2(R) CALC_VERSION_STR(R)

/**
 * @brief 应用程序配置
 */
class AppConfig : public QObject {
    Q_OBJECT

public:
    static AppConfig* getInstance();

private:
    AppConfig();
    static AppConfig* app_config_self;

public:
    /**
     * @brief 读取配置文件
     */
    void loadConfigFile();
    void initFontConfig();
    void applyConfig(const QMap<QString, QString>& configMap);

public:
    QString appVersion() {
        return this->m_app_version;
    }
    [[nodiscard]] int appVersionMajor() const {
        return this->m_app_version_major;
    }
    [[nodiscard]] int appVersionMinor() const {
        return this->m_app_version_major;
    }
    [[nodiscard]] int appVersionPatch() const {
        return this->m_app_version_major;
    }
    spdlog::level::level_enum logLevel() {
        return this->m_log_level;
    }

    [[nodiscard]] bool configBehaviorSaveModelOnExist() const {
        return this->m_config_behavior_save_model_on_exist;
    }
    void configBehaviorSaveModelOnExist(bool val) {
        this->m_config_behavior_save_model_on_exist = val;
    }

    [[nodiscard]] bool configBehaviorSaveDefineOnExist() const {
        return this->m_config_behavior_save_define_on_exist;
    }
    void configBehaviorSaveDefineOnExist(bool val) {
        this->m_config_behavior_save_define_on_exist = val;
    }

    [[nodiscard]] bool configBehaviorClearHistoryOnExist() const {
        return this->m_config_behavior_clear_history_on_exist;
    }
    void configBehaviorClearHistoryOnExist(bool val) {
        this->m_config_behavior_clear_history_on_exist = val;
    }

    [[nodiscard]] bool configBehaviorMultiInstance() const {
        return this->m_config_behavior_multi_instance;
    }
    void configBehaviorMultiInstance(bool val) {
        this->m_config_behavior_multi_instance = val;
    }

    [[nodiscard]] bool configAppearanceRememberWindowPosition() const {
        return this->m_config_appearance_remember_window_position;
    }
    void configAppearanceRememberWindowPosition(bool val) {
        this->m_config_appearance_remember_window_position = val;
    }

    [[nodiscard]] QString configAppearanceWindowTitle() const {
        return this->m_config_appearance_window_title;
    }
    void configAppearanceWindowTitle(const QString& val) {
        this->m_config_appearance_window_title = val;
    }

    [[nodiscard]] int configAppearanceExpressionLine() const {
        return this->m_config_appearance_expression_line;
    }
    void configAppearanceExpressionLine(int val) {
        this->m_config_appearance_expression_line = val;
    }

    [[nodiscard]] QFont configFontColorFontResult() const {
        return this->m_config_font_color_font_result;
    }
    [[nodiscard]] QFont configFontColorFontExpression() const {
        return this->m_config_font_color_font_expression;
    }
    [[nodiscard]] QFont configFontColorFontStatus() const {
        return this->m_config_font_color_font_status;
    }
    [[nodiscard]] QColor configFontColorColorWord() const {
        return this->m_config_font_color_color_word;
    }
    [[nodiscard]] QColor configFontColorColorError() const {
        return this->m_config_font_color_color_error;
    }
    [[nodiscard]] QColor configFontColorColorWarn() const {
        return this->m_config_font_color_color_warn;
    }

private:
    /**
     * @brief 保存配置文件
     */
    void saveConfigFile();

private:
    // app 版本信息
    QString m_app_version;
    int     m_app_version_major;
    int     m_app_version_minor;
    int     m_app_version_patch;
    /**
     * @brief 日志级别
     */
    spdlog::level::level_enum m_log_level;

    QDir m_app_dir;

    // 配置文件位置
    QString m_config_file_path;

    // 退出时保存模式
    bool m_config_behavior_save_model_on_exist{true};
    // 退出时保存定义
    bool m_config_behavior_save_define_on_exist{true};
    // 退出时清除历史记录
    bool m_config_behavior_clear_history_on_exist{false};
    // 允许多个实例
    bool m_config_behavior_multi_instance{false};

    // 记住窗口位置
    bool m_config_appearance_remember_window_position{false};
    // 窗口标题
    // appname: 应用程序名称
    // result: 结果
    // appname_result: 应用程序名称+结果
    // model: 模式
    // appname_model: 应用程序名称+模式
    QString m_config_appearance_window_title{"appname"};
    // 表达式行数
    int m_config_appearance_expression_line{3};

    // 结果字体
    QFont m_config_font_color_font_result{QFontDatabase::systemFont(QFontDatabase::GeneralFont)};
    // 表达式字体
    QFont m_config_font_color_font_expression{QFontDatabase::systemFont(QFontDatabase::GeneralFont)};
    // 状态字体
    QFont m_config_font_color_font_status{QFontDatabase::systemFont(QFontDatabase::GeneralFont)};
    // 文字颜色
    QColor m_config_font_color_color_word{"#000000"};
    // 错误状态颜色
    QColor m_config_font_color_color_error{"#7F0000"};
    // 警告状态颜色
    QColor m_config_font_color_color_warn{"#0000B2"};
};
};  // namespace calc
#endif
