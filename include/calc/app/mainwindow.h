/**
 * @file mainwindow.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 主窗口
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <spdlog/logger.h>

#include <QAction>
#include <QMainWindow>
#include <QMenu>

#include "calc/app/aboutdialog.h"
#include "calc/app/preferencedialog.h"

namespace calc {

/**
 * @brief 主窗口
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override = default;

private:
    void initMenu();

private:
    AboutDialog      *m_about_dialog{nullptr};
    PreferenceDialog *m_preference_dialog{nullptr};

private:
    QMenu *m_menu_file{nullptr};
    QMenu *m_menu_edit{nullptr};
    QMenu *m_menu_model{nullptr};
    QMenu *m_menu_func{nullptr};
    QMenu *m_menu_variable{nullptr};
    QMenu *m_menu_unit{nullptr};
    QMenu *m_menu_help{nullptr};

    QAction *m_act_edit_copy_result{nullptr};
    QAction *m_act_edit_keys{nullptr};
    QAction *m_act_edit_custom_keys{nullptr};
    QAction *m_act_edit_preference{nullptr};

    QAction *m_act_help_content{nullptr};
    QAction *m_act_help_report{nullptr};
    QAction *m_act_help_check_update{nullptr};
    QAction *m_act_help_about{nullptr};
    QAction *m_act_help_about_qt{nullptr};

private:
    std::shared_ptr<spdlog::logger> log;
};

};  // namespace calc

#endif  // MAINWINDOW_H
