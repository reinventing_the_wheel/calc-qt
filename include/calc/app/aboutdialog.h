/**
 * @file aboutdialog.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 关于对话框
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef CALC_ABOUT_DIALOG_CONFIG_H_
#define CALC_ABOUT_DIALOG_CONFIG_H_

#include <QDialog>

namespace calc {

/**
 * @brief 关于对话框
 */
class AboutDialog : public QDialog {
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = nullptr);
    ~AboutDialog() override = default;

private:
    void initLayout();

private:
};

};  // namespace calc

#endif  // MAINWINDOW_H
