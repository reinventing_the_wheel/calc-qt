/**
 * @file preferencedialog.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 首选项对话框
 * @version 1.0.0
 * @date 2024-02-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef CALC_PREFERENCE_DIALOG_CONFIG_H_
#define CALC_PREFERENCE_DIALOG_CONFIG_H_

#include <spdlog/logger.h>

#include <QCheckBox>
#include <QColorDialog>
#include <QComboBox>
#include <QDialog>
#include <QFontDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QMap>
#include <QMessageBox>
#include <QPushButton>
#include <QShowEvent>
#include <QSpinBox>
#include <QString>
#include <QTabWidget>
#include <QVBoxLayout>

namespace calc {

/**
 * @brief 首选项面板基类
 */
class PreferenceDialogPanel : public QWidget {
    Q_OBJECT
protected:
    /**
     * @brief 已改变的选项
     */
    QMap<QString, QString> m_config_changed_map;

public:
    /**
     * @brief tab 标题
     * 
     * @return tab 标题
     */
    virtual QString tabTitle() = 0;
    /**
     * @brief 是否改变了选项
     * 
     * @return true 改变了选项
     * @return false 未改变选项
     */
    bool hasConfigChanged();
    /**
     * @brief 保存设置
     */
    void saveConfig();
    /**
     * @brief 重置设置
     */
    virtual void resetConfig() = 0;
};

/**
 * @brief 行为选项配置面板
 */
class PreferenceDialogBehaviorPanel : public PreferenceDialogPanel {
    Q_OBJECT
public:
    PreferenceDialogBehaviorPanel();
    ~PreferenceDialogBehaviorPanel() override = default;

public:
    QString tabTitle() override;
    void    resetConfig() override;

private:
    std::shared_ptr<spdlog::logger> log;

    /**
     * @brief 退出时保存模式
     */
    QCheckBox *m_save_model_on_exist{nullptr};
    /**
     * @brief 退出时保存定义
     */
    QCheckBox *m_save_define_on_exist{nullptr};
    /**
     * @brief 退出时清除历史记录
     */
    QCheckBox *m_clear_history_on_exist{nullptr};
    /**
     * @brief 允许多个实例
     */
    QCheckBox *m_multi_instance{nullptr};

private:
    /**
     * @brief 初始化布局
     */
    void initLayout();
    /**
     * @brief 初始化组件信号与槽连接
     */
    void initConnect();
};

/**
 * @brief 外观选项配置面板
 */
class PreferenceDialogAppearancePanel : public PreferenceDialogPanel {
    Q_OBJECT
public:
    PreferenceDialogAppearancePanel();
    ~PreferenceDialogAppearancePanel() override = default;

public:
    QString tabTitle() override;
    void    resetConfig() override;

private:
    std::shared_ptr<spdlog::logger> log;
    /**
     * @brief 窗口标题,索引 缓存
     */
    QMap<QString, int> m_title_val_index_map;
    /**
     * @brief 索引,窗口标题 缓存
     */
    QMap<int, QString> m_title_index_val_map;
    /**
     * @brief 记住窗口位置
     */
    QCheckBox *m_remember_window_position_checkbox{nullptr};
    /**
     * @brief 窗口标题
     */
    QComboBox *m_window_title_combobox{nullptr};
    /**
     * @brief 表达式行数
     */
    QSpinBox *m_expression_spinbox{nullptr};

private:
    /**
     * @brief 初始化布局
     */
    void initLayout();
    /**
     * @brief 初始化组件信号与槽连接
     */
    void initConnect();
};

/**
 * @brief 字体与颜色选项配置面板
 * 
 */
class PreferenceDialogFontColorPanel : public PreferenceDialogPanel {
    Q_OBJECT
public:
    PreferenceDialogFontColorPanel();
    ~PreferenceDialogFontColorPanel() override = default;

public:
    QString tabTitle() override;
    void    resetConfig() override;

private:
    std::shared_ptr<spdlog::logger> log;

    /**
     * @brief 是否自定义结果字体
     */
    QCheckBox *m_check_font_result{nullptr};
    /**
     * @brief 结果字体
     */
    QPushButton *m_btn_font_result{nullptr};
    /**
     * @brief 是否自定义表达式字体
     */
    QCheckBox *m_check_font_expression{nullptr};
    /**
     * @brief 表达式字体
     */
    QPushButton *m_btn_font_expression{nullptr};
    /**
     * @brief 是否自定义状态字体
     */
    QCheckBox *m_check_font_status{nullptr};
    /**
     * @brief 状态字体
     */
    QPushButton *m_btn_font_status{nullptr};
    /**
     * @brief 文字颜色
     */
    QPushButton *m_btn_color_word{nullptr};
    /**
     * @brief 错误状态颜色
     */
    QPushButton *m_btn_color_error{nullptr};
    /**
     * @brief 警告状态颜色
     */
    QPushButton *m_btn_color_warn{nullptr};

private:
    /**
     * @brief 初始化布局
     */
    void initLayout();
    /**
     * @brief 初始化组件信号与槽连接
     */
    void initConnect();
};

/**
 * @brief 首选项对话框
 */
class PreferenceDialog : public QDialog {
    Q_OBJECT

public:
    explicit PreferenceDialog(QWidget *parent = nullptr);
    ~PreferenceDialog() override = default;

private:
    /**
     * @brief 初始化布局
     */
    void initLayout();

protected:
    void showEvent(QShowEvent *e) override;

private:
    std::shared_ptr<spdlog::logger> log;

    /**
     * @brief 面板 tab 容器
     */
    QTabWidget *m_tab{nullptr};
    /**
     * @brief 当前激活的 tab 索引
     */
    int m_curr_tab_index{0};
    /**
     * @brief 配置面板列表
     */
    QList<PreferenceDialogPanel *> m_panels;
};

};  // namespace calc

#endif  // MAINWINDOW_H
