#include <QtTest>
#include <memory>

#include "calc/util/log_util.h"

class TestMain : public QObject {
    Q_OBJECT
public:
    TestMain() {
        calc::LogUtil::init(calc::AppConfig::getInstance()->logLevel(), "../logs/test.log");
    }
private slots:
    void testCommon() {  // NOLINT
        std::shared_ptr<spdlog::logger> log = calc::LogUtil::getLogger("TestRapidJson");
        SPDLOG_LOGGER_INFO(log, fmt::format("test common"));
        auto appConfig = calc::AppConfig::getInstance();
        SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[appVersion]:{}", appConfig->appVersion().toStdString()));
        QVERIFY(1 == 1);
    }
};

QTEST_MAIN(TestMain)
#include "testmain.moc"
