# calc-qt

# 一个例子 Qalculate!

## 官网

[https://qalculate.github.io/index.html](https://qalculate.github.io/index.html)

## 安装

```shell
sudo apt-get install qalculate-gtk
```

安装之后默认就是中文界面,且有本地的帮助文档

# 依赖库

## pugixml

官网: [https://pugixml.org/](https://pugixml.org/)

中文文: [pugixml 1.10 快速入门指南](https://www.cnblogs.com/Gale-Tech/p/16089326.html)
